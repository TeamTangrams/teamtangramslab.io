var noadsver = "$Id: no-ads.pac,v 6.3 2017/09/28 17:08:39 loverso Exp loverso $";

var normal = "DIRECT";

//var blackhole = "PROXY 127.0.0.1:3421";
var blackhole = "PROXY 127.0.0.1:3421";
// Safari/MacOS needs the old value
if (typeof(navigator) != "undefined"
	&& navigator.appVersion.indexOf("Mac") != -1) {
    blackhole = "PROXY 0.0.0.0:3421";
}


var localproxy = normal;

var socksproxy = normal;

var bypass = normal;



var isActive = 1;

function FindProxyForURL(url, host)
{
    url = url.toLowerCase();
    host = host.toLowerCase();

    //
    // Local/Internal rule
    // matches to this rule get the 'local' proxy.
    // Adding rules here enables the use of 'local'
    //
    if (0
	//LOCAL-RULES
	// add rules such as:
  	//	|| dnsDomainIs(host, "schooner.com")
	//	|| isPlainHostName(host)
	// or for a single host
	//	|| (host == "some-local-host")
    ) {
	//LOG3 alert("no-ads local: " + url);
	return localproxy;
    }

    //hosts to push out to a SOCKS proxy
    if (0
	//SOCKS-RULES
    ) {
	//LOG3 alert("no-ads socks: " + url);
	return socksproxy;
    }

    
	if (0

	|| _dnsDomainIs(host, "teamtangrams.gitlab.io/zenTest.html")

	) {
	//LOG2 alert("no-ads blocking: " + url);

	// deny this request
	return blackhole;

    } else {
	//LOG3 alert("no-ads allowing: " + url);

	// all other requests go direct and avoid any overhead
	return normal;
    }
}

///////////////////////////////////////////////////////////////////////////////
//
// This line is just for testing; you can ignore it.  But, if you are having
// problems where you think this PAC file isn't being loaded, then change this
// to read "if (1)" and the alert box should appear when the browser loads this
// file.
//
// This works for IE4, IE5, IE5.5, IE6 and Netscape 2.x, 3.x, and 4.x.
// (For IE6, tested on Win2K)
// This does not work for Mozilla before 1.4 (and not for Netscape 6.x).
// In Mozilla 1.4+ and Fireox, this will write to the JavaScript console.
//
if (0) {
	alert("no-ads.pac: LOADED:\n" +
		"	version:	"+noadsver+"\n" +
		"	blackhole:	"+blackhole+"\n" +
		"	normal:		"+normal+"\n" +
		"	localproxy:	"+localproxy+"\n" +
		"	bypass:		"+bypass+"\n"
		//MSG
	);
}

// The above should show you that this JavaScript is executed in an
// unprotected global context.  NEVER point at someone elses autoconfig file;
// always load from your own copy!

// an alert that returns true (normally it returns void)
function alertmatch(str)
{
	alert(str);
	return 1;
}

///////////////////////////////////////////////////////////////////////////////
//
// Replacement function for dnsDomainIs().  This is to replace the
// prefix problem, which a leading '.' used to be used for.
//
//	dnsDomainIs("bar.com", "bar.com") => true
//	dnsDomainIs("www.bar.com", "bar.com") => true
//	dnsDomainIs("www.foobar.com", "bar.com") => true	<<< incorrect
//
//	_dnsDomainIs("bar.com", "bar.com") => true
//	_dnsDomainIs("www.bar.com", "bar.com") => true
//	_dnsDomainIs("www.foobar.com", "bar.com") => false	<<< correct
//
// 2016 update: Firefox 47 is broken:
//
//	dnsDomainIs("bar.com", ".bar.com") => false		<<< incorrect
//
function _dnsDomainIs(host, domain) {
    if (host.length > domain.length) {
	return (host.substring(host.length - domain.length - 1) == "."+domain);
    }
    return (host == domain);
}

///////////////////////////////////////////////////////////////////////////////
//
// Tired of reading boring comments?  Try reading today's comics:
//	http://www.schooner.com/~loverso/comics/
//
// or getting a quote from my collection:
//	http://www.schooner.com/~loverso/quote/
//

// eof
	//intelliserv.net
	//intellisrv.net
	//rambler.ru
	//rightmedia.net
	//calloffate.com
	//fairmeasures.com

